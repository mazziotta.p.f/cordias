
import SVG from 'svg.js';

class DrawingEditor {

  _canvas;
  shapes;
  tools;
  selectedShape;

  constructor() {
    this.tools = {
      drag: new ReactiveVar(false),
      resize: new ReactiveVar(false),
      draw: new ReactiveVar(false),
    };
    this.selectedShape = new ReactiveVar(null);
    this.reset();
  }

  reset(elementId, width, height) {
    if (!elementId) elementId = 'drawing';
    if (!width) width = '100%';
    if (!height) height = '100%';
    this.buildCanvas(elementId, width, height);
    this.shapes = {};
    this.selectedShape.set(null);
    this.deactivateTools();
  }

  buildCanvas(elementId, width, height) {
    if (!document.getElementById(elementId)) return;
    this.canvas = SVG(elementId).size(width, height);
  }

  get canvas() {
    return this._canvas;
  }

  set canvas(value) {
    if (this._canvas && this._canvas.type == 'svg') this.canvas.remove();
    this._canvas = value;
  }

  aShapeIsSelected() {
    return this.selectedShape.get() ? true: false;
  }

  removeSelected() {
    if (this.aShapeIsSelected()) {
      this.selectedShape.get().remove();
      this.deactivateTool('resize');
    }
  }

  toolIsActive(toolName) {
    return this.tools[toolName].get();
  }

  toggleTool(toolName) {
    if (this.toolIsActive(toolName))
      this.deactivateTool(toolName);
    else
      this.activateTool(toolName);
  }

  activateTool(toolName) {
    this.deactivateTools();
    this.tools[toolName].set(true);
    if (toolName === 'drag') this.toggleDraggability();
    else if (toolName === 'draw') this.drawPolygon();
  }

  deactivateTool(toolName) {
    this.tools[toolName].set(false);
    if (this.selectedShape.get()) this.unselectSelectedShape();
    if (toolName === 'drag') this.toggleDraggability();
  }

  deactivateTools() {
    Object.keys(this.tools)
      .forEach((toolName) => { this.deactivateTool(toolName) });
  }

  addShape(shape) {
    this.shapes[shape.id()] = shape;
  }

  resizeShape(shapeId) {
    if (!this.toolIsActive('resize')) return;
    if (this.selectedShape.get()) this.unselectSelectedShape();
    let shape = this.shapes[shapeId]
    if (shape) {
      this.selectedShape.set(shape);
      shape.selectize().selectize({deepSelect:true}).resize().draggable();
    }
  }

  unselectShape(shape) {
    shape.selectize(false).selectize(false, {deepSelect:true}).resize('stop').draggable(false);
    this.selectedShape.set(null);
  }

  unselectSelectedShape() {
    this.unselectShape(this.selectedShape.get());
  }

  unselectAllShapes() {
    Object.values(this.shapes).forEach((shape) => {
      this.unselectShape(shape);
    });
  };

  toggleDraggability() {
    if (this.toolIsActive('drag')) {
      Object.values(this.shapes).forEach((shape) => {
        shape.draggable();
      });
    }
    else {
      Object.values(this.shapes).forEach((shape) => {
        shape.draggable(false);
      });
    }
  }

  drawPolygon(strokeColor, fillColor) {
    let polygon = this.canvas.polygon().draw();
    let opacity = 0.2;
    let width = 3;

    if (strokeColor) polygon.stroke({ color: strokeColor, width });
    else polygon.stroke({ color: 'black', width });
    if (fillColor) polygon.fill({ color: fillColor, opacity });
    else polygon.fill({ color: 'black', opacity });

    polygon
      .attr('pointer-events', 'visible')
      .attr('data-drawing-action', 'resize-shape');

    let listener = (event) => {
      if(event.keyCode == 13){
        polygon.draw('done');
        polygon.off('drawstart');
        if (polygon.array().value.length > 1)
          this.addShape(polygon);
        else
          polygon.remove();
        this.deactivateTool('draw');
      }
    };

    polygon.on('drawstart', (event) => {
      document.addEventListener('keypress', listener);
    });

    polygon.on('drawstop', () => {
      document.removeEventListener('keypress', listener)
    });
  }
}

Drawing = new DrawingEditor();
