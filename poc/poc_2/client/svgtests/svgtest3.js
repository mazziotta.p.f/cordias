
Template.Svgtest3.onRendered(function () {
  Drawing.reset();

  Drawing.canvas = SVG('drawing').size('100%', '100%');
  var rect = Drawing.canvas.rect(100, 100).attr({ fill: '#f06' })
    .attr('pointer-events', 'visible')
    .attr('data-drawing-action', 'resize-shape');
  var rect2 = Drawing.canvas.rect(50, 50).attr({ fill: '#0f6' })
    .attr('pointer-events', 'visible')
    .attr('data-drawing-action', 'resize-shape');

  Drawing.addShape(rect);
  Drawing.addShape(rect2);

  rect2.move(100, 100);
});

Template.Svgtest3.events({
  'mousedown [data-drawing-action="resize-shape"]'(event, template) {
    Drawing.resizeShape(event.target.id);
  },
  'click [data-drawing-action="activate-dragging"]'(event, template) {
    Drawing.toggleTool('drag');
  },
  'click [data-drawing-action="activate-resizing"]'(event, template) {
    Drawing.toggleTool('resize');
  },
});

Template.Svgtest3.helpers({
  dragToolIsActive: () => Drawing.toolIsActive('drag'),
  resizeToolIsActive: () => Drawing.toolIsActive('resize'),
});
