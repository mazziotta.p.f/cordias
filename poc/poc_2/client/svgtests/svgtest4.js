
Template.Svgtest4.onRendered(function () {
  Drawing.reset('drawing');
});

Template.Svgtest4.events({
  'mousedown [data-drawing-action="resize-shape"]'(event, template) {
    Drawing.resizeShape(event.target.id);
  },
  'click [data-drawing-action="activate-dragging"]'(event, template) {
    Drawing.toggleTool('drag');
  },
  'click [data-drawing-action="activate-resizing"]'(event, template) {
    Drawing.toggleTool('resize');
  },
  'click [data-drawing-action="activate-drawing"]'(event, template) {
    Drawing.toggleTool('draw');
  },
  'click [data-drawing-action="remove-selected"]'(event, template) {
    Drawing.removeSelected();
  },
});

Template.Svgtest4.helpers({
  dragToolIsActive: () => Drawing.toolIsActive('drag'),
  resizeToolIsActive: () => Drawing.toolIsActive('resize'),
  drawToolIsActive: () => Drawing.toolIsActive('draw'),
  shapeIsSelected: () => Drawing.aShapeIsSelected(),
});
