
Template.Home.events({
  'keyup [data-action="update-name"]'(event, template) {
    const input = event.target;
    if (event.which === 13)
      input.parentElement.removeChild(input);
    else
      Meteor.call('persons.updateName', this._id, input.value);
  },
  'click [data-view-action="toggle-input"]'(event, template) {
    template.findAll('[data-action="update-name"]').forEach((input) => {
      input.parentElement.removeChild(input);
    });

    let input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('data-action', 'update-name');
    input.setAttribute('value', this.name);

    event.target.appendChild(input);
    input.focus();
  },
});
