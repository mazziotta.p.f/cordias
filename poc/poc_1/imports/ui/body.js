
import './style/default.css';
import './templates/home.html';

import './body.helpers.js';
import './body.events.js';

import './body.routes.js';
