
import sigma from 'sigma';

import { Persons } from '/imports/api/persons.js';

Router.route('/', {
  name: 'home',
  template: 'Home',
  subscriptions: function() {
    Meteor.subscribe('personsWithReportingHierarchyUsingReactiveAggregation', () => {
      var s = new sigma({
        graph: Router.current().data().graph(),
        container: 'graph',
        settings: {
          labelThreshold: 1,
          zoomMin: 1,
          zoomMax: 1,
        }
      });
    });
  },
  data: {
    persons: () => Persons.find(),
    graph: () => {
      let persons = Persons.find();
      let graph = {nodes: [], edges: []};
      i = 0;
      n = Persons.find().count();
      pi = Math.PI;

      persons.forEach((person) => {
        i++;
        graph.nodes.push({
          id: 'n:' + person._id,
          label: person.name,
          x: Math.cos(pi * 2 * i / n - pi / 2 ),
          y: Math.sin(pi * 2 * i / n - pi / 2 ),
          size: 1 + person.reportingHierarchy.length,
          color: '#FF0000',
        });

        person.reportingHierarchy.forEach((element) => {
          graph.edges.push({
            id: 'n:' + person._id + '_e:' + element._id,
            source: 'n:' + person._id,
            target: 'n:' + element._id,
            size: 1,
            color: '#0000FF',
          });
        });
      });

      return graph;
    }
  },
});
