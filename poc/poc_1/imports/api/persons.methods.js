
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Persons } from '/imports/api/persons.js';

Meteor.methods({
  'persons.create'(name) {

    check(name, String);

    Persons.insert({
      name,
    });
  },
  'persons.addRelation'(a, tag, b) {

    check(a, String);
    check(b, String);

    let tagObject = {};
    tagObject[tag] = b;

    Persons.update({ _id: a }, {$set: tagObject});
  },
  'persons.updateName'(personId, name) {

    check(personId, String);
    check(name, String);

    Persons.update({ _id: personId }, {$set: {name}});
  },
});
