
import { Meteor } from 'meteor/meteor';

import { Persons } from '/imports/api/persons.js';

Meteor.publish('personsWithReportingHierarchyUsingRawCollection', function () {

  const db = Persons.rawCollection();
  const aggregate = Meteor.wrapAsync(db.aggregate, db);

  return aggregate([
   {
      $graphLookup: {
         from: "persons",
         startWith: "$reportsTo",
         connectFromField: "reportsTo",
         connectToField: "_id",
         as: "reportingHierarchy"
      }
    }
  ]).forEach((person) => { this.added('persons', person._id, person); });
});

Meteor.publish('personsWithReportingHierarchyUsingReactiveAggregation', function () {

  return ReactiveAggregate(this, Persons, [
   {
      $graphLookup: {
         from: "persons",
         startWith: "$reportsTo",
         connectFromField: "reportsTo",
         connectToField: "_id",
         as: "reportingHierarchy"
      }
    }
  ]);
});
