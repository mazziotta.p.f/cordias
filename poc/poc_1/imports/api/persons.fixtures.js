
import { Persons } from '/imports/api/persons.js';

Meteor.startup(() => {
  if (Persons.find().count() === 0) {
    Meteor.call('persons.create', 'Dev');
    Meteor.call('persons.create', 'Eliot');
    Meteor.call('persons.create', 'Ron');
    Meteor.call('persons.create', 'Andrew');
    Meteor.call('persons.create', 'Asya');
    Meteor.call('persons.create', 'Dan');

    let dev = Persons.findOne({name: 'Dev'});
    let eliot = Persons.findOne({name: 'Eliot'});
    let ron = Persons.findOne({name: 'Ron'});
    let andrew = Persons.findOne({name: 'Andrew'});
    let asya = Persons.findOne({name: 'Asya'});
    let dan = Persons.findOne({name: 'Dan'});

    Meteor.call('persons.addRelation', eliot._id, 'reportsTo', dev._id);
    Meteor.call('persons.addRelation', ron._id, 'reportsTo', eliot._id);
    Meteor.call('persons.addRelation', andrew._id, 'reportsTo', eliot._id);
    Meteor.call('persons.addRelation', asya._id, 'reportsTo', ron._id);
    Meteor.call('persons.addRelation', dan._id, 'reportsTo', andrew._id);
  }
});
