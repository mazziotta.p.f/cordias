import '/imports/api/persons.js';
import '/imports/api/persons.fixtures.js';
import '/imports/api/persons.methods.js';
import '/imports/api/persons.publications.js';

import '/imports/ui/body.routes.js';
