
Template.Fusekitest1.onCreated(function() {
  Template.Fusekitest1.fuzekiData = new ReactiveVar();
  let query =
    "SELECT ?subject ?predicate ?object "+
    "WHERE { "+
    "  ?subject ?predicate ?object "+
    "} "+
    "LIMIT 5 ";
  Template.Fusekitest1.fuzekiQuery = query;
  HTTP.call('POST', 'http://localhost:3030/poc_3/sparql?query=' + query, {
    headers: {
      'Accept': 'application/sparql-results+json; charset=utf-8',
      'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
    },
  }, (error, result) => {
    if (!error) {
      Template.Fusekitest1.fuzekiData.set(JSON.parse(result.content));
    }
  });
});

Template.Fusekitest1.helpers({
  fusekiCallResult: () => Template.Fusekitest1.fuzekiData.get(),
  query: () => Template.Fusekitest1.fuzekiQuery,
});

Template.Fusekitest2.onCreated(function() {
  Template.Fusekitest2.fuzekiData = new ReactiveVar();
  HTTP.call('GET', 'http://localhost:3000/fuseki/simpleQuery', {
    headers: {
      'Accept': 'application/json; charset=utf-8',
    },
  }, (error, result) => {
    if (!error) {
      Template.Fusekitest2.fuzekiData.set(JSON.parse(result.content));
    }
  });
});

Template.Fusekitest2.helpers({
  fusekiCallResult: () => Template.Fusekitest2.fuzekiData.get(),
});

Template.Fusekitest3.onCreated(function() {
  Template.Fusekitest3.fuzekiData = new ReactiveVar();
  Template.Fusekitest3.personsWithAge = new ReactiveVar();
  Template.Fusekitest3.populate = function() {
    HTTP.call('GET', 'http://localhost:3000/fuseki/persons', {
      headers: {
        'Accept': 'application/json; charset=utf-8',
      },
    }, (error, result) => {
      if (!error) {
        Template.Fusekitest3.fuzekiData.set(JSON.parse(result.content));
      }
    });
  };
  Template.Fusekitest3.populatePersonsWithAge = function() {
    HTTP.call('GET', 'http://localhost:3000/fuseki/personsWithAge', {
      headers: {
        'Accept': 'application/json; charset=utf-8',
      },
    }, (error, result) => {
      if (!error) {
        let content = JSON.parse(result.content);
        content.results.bindings.forEach(
          (person) => { person.depth = 0; }
        );
        Template.Fusekitest3.personsWithAge.set(content);
      }
    });
  };

  Template.Fusekitest3.populate();
  Template.Fusekitest3.populatePersonsWithAge();
});

Template.Fusekitest3.helpers({
  fusekiCallResult: () => Template.Fusekitest3.fuzekiData.get(),
  personsWithAge: () => Template.Fusekitest3.personsWithAge.get(),
});

Template.Fusekitest3.events({
  'submit [data-action="insert-triplet"]'(event, template) {
    event.preventDefault();
    const target = event.target;

    Meteor.call('fuseki.insertPerson',
      target.name.value,
      parseInt(target.age.value),
      () => {
        Template.Fusekitest3.populate();
        Template.Fusekitest3.populatePersonsWithAge();
      });
    target.name.value = '';
    target.age.value = '';
  },
  'click [data-action="delete"]'(event, template) {
    Meteor.call('fuseki.remove',
      this.subject,
      this.predicate,
      this.object,
      () => {
        Template.Fusekitest3.populate();
        Template.Fusekitest3.populatePersonsWithAge();
      });
  },
});

MAX_DEPTH = 5;

Template.Fusekitest3children.onCreated(function() {
  this.state = new ReactiveVar();

  this.populate = () => {
    let depth = this.data.depth;
    if (depth >= MAX_DEPTH) return;
    HTTP.call('GET', 'http://localhost:3000/fuseki/children/' + this.data.name.value, {
      headers: {
        'Accept': 'application/json; charset=utf-8',
      },
    }, (error, result) => {
      if (!error) {
        let content = JSON.parse(result.content);
        content.results.bindings.forEach(
          (person) => { person.depth = depth + 1; }
        );
        this.state.set(content);
      }
    });
  };

  this.populate();
});

Template.Fusekitest3children.helpers({
  children: function() {
    return Template.instance().state.get();
  },
  personsWithAge: () => Template.Fusekitest3.personsWithAge.get(),
});

Template.Fusekitest3children.events({
  'submit [data-action="add-child"]'(event, template) {
    event.preventDefault();
    const target = event.target;

    Meteor.call('fuseki.addChild',
      this.name.value,
      target.child.selectedOptions[0].value,
      () => {
        Template.Fusekitest3.populate();
        template.populate();
      });
  },
});

Router.route('/', {
  name: 'home',
});

Router.route('fusekitest1');
Router.route('fusekitest2');
Router.route('fusekitest3');
