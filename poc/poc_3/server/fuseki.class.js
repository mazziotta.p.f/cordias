class FusekiDatabaseAccessor {
  insertPerson(name, age) {
    // TODO : prevent injections
    let query =
      'PREFIX dc: <http://purl.org/dc/elements/1.1/> ' +
      'INSERT DATA ' +
      '{ ' +
      '  <http://example/person/' + name + '> ' +
      '    dc:type "person" ;' +
      '    dc:name "' + name + '" ;' +
      '    dc:age "' + age + '" .' +
      '}';

    return this.query(query, 'poc_3_persons', 'update');
  }

  prepareParameter(parameter) {
    // TODO : prevent injections
    switch (parameter.type) {
      case 'uri':
        return '<' + parameter.value + '>';
        break;
      case 'literal':
        return '"' + parameter.value + '"';
        break;
      default:
        throw new Meteor.Error(500, 'Error 500: unrecognized parameter')
    }
  }

  remove(subject, predicate, object) {
    let query =
      'DELETE DATA ' +
      '{ ' +
      '  ' + this.prepareParameter(subject) + ' '
           + this.prepareParameter(predicate) + ' '
           + this.prepareParameter(object) + ' .' +
      '}';

    return this.query(query, 'poc_3_persons', 'update');
  }

  findPersonsWithAge() {
    let query =
      'PREFIX dc: <http://purl.org/dc/elements/1.1/> ' +
      'SELECT ?name ?age ' +
      'WHERE { ' +
      '  ?person dc:type "person" .' +
      '  ?person dc:name ?name . ' +
      '  OPTIONAL { ?person dc:age ?age } ' +
      '}';

    return this.query(query, 'poc_3_persons');
  }

  addParentToPerson(parentName, childName) {
    if (parentName === childName)
      throw new Meteor.Error(500, 'Error 500: A person cannot be his own parent');
    let query =
      'PREFIX dc: <http://purl.org/dc/elements/1.1/> ' +
      'INSERT DATA ' +
      '{ ' +
      '  <http://example/person/' + parentName + '> ' +
      '    dc:parentOf <http://example/person/' + childName + '> .' +
      '}';

    return this.query(query, 'poc_3_persons', 'update');
  }

  getPersons() {
    let query =
      "SELECT ?subject ?predicate ?object "+
      "WHERE { "+
      "  ?subject ?predicate ?object "+
      "} ";

    return this.query(query, 'poc_3_persons');
  }

  getChildren(personName) {
    // TODO : prevent injections
    let query =
      'PREFIX dc: <http://purl.org/dc/elements/1.1/> ' +
      'SELECT ?name ?age ' +
      'WHERE { ' +
      '  ?person dc:name "' + personName + '" . ' +
      '  ?person dc:parentOf ?child . ' +
      '  ?child dc:name ?name . ' +
      '  ?child dc:age ?age ' +
      '}';

    return this.query(query, 'poc_3_persons');
  }

  getData() {
    let query =
      "SELECT ?subject ?predicate ?object "+
      "WHERE { "+
      "  ?subject ?predicate ?object "+
      "} "+
      "LIMIT 5 ";

    return this.query(query, 'poc_3');
  }

  query(query, dataset, queryType) {
    if (!queryType) queryType = 'query';
    const path = 'http://localhost:3030/' +
      dataset + '/' + queryType + '?' +
      queryType + '=' + query;

    try {
      return HTTP.call('POST', path, {
        headers: {
          'Accept': 'application/sparql-results+json; charset=utf-8',
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        },
      });
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}

Fuseki = new FusekiDatabaseAccessor();
