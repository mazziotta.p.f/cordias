
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
  'fuseki.simpleQuery'() {
    this.unblock();
    let result = Fuseki.getData();
    if (!result || !result.content)
      throw new Meteor.Error(400, 'Error 400: your query did not return anything');
    if (result.statusCode === 200)
      return result.content;
    else
      throw new Meteor.Error(result.statusCode, result.content);
  },
  'fuseki.persons'() {
    this.unblock();
    let result = Fuseki.getPersons();
    if (!result || !result.content)
      throw new Meteor.Error(400, 'Error 400: your query did not return anything');
    if (result.statusCode === 200)
      return result.content;
    else
      throw new Meteor.Error(result.statusCode, result.content);
  },
  'fuseki.children'(personName) {
    this.unblock();
    let result = Fuseki.getChildren(personName);
    if (!result || !result.content)
      throw new Meteor.Error(400, 'Error 400: your query did not return anything');
    if (result.statusCode === 200)
      return result.content;
    else
      throw new Meteor.Error(result.statusCode, result.content);
  },
  'fuseki.personsWithAge'() {
    this.unblock();
    let result = Fuseki.findPersonsWithAge();
    if (!result || !result.content)
      throw new Meteor.Error(400, 'Error 400: your query did not return anything');
    if (result.statusCode === 200)
      return result.content;
    else
      throw new Meteor.Error(result.statusCode, result.content);
  },
  'fuseki.insertPerson'(name, age) {
    check(name, String);
    check(age, Number);

    Fuseki.insertPerson(name, age);
  },
  'fuseki.remove'(subject, predicate, object) {
    check(subject, Object);
    check(predicate, Object);
    check(object, Object);

    Fuseki.remove(subject, predicate, object);
  },
  'fuseki.addChild'(parentName, childName) {
    check(parentName, String);
    check(childName, String);

    Fuseki.addParentToPerson(parentName, childName);
  },
});
