
Router.route('/fuseki/simpleQuery', { where: 'server' })
  .get(function () {
    Meteor.call('fuseki.simpleQuery', (error, result) => {
      if (!error) {
        this.response.statusCode = 200;
        this.response.end(result);
      } else {
        this.response.statusCode = error.error;
        this.response.end(error.reason);
      }
    });
  });

Router.route('/fuseki/persons', { where: 'server' })
  .get(function () {
    Meteor.call('fuseki.persons', (error, result) => {
      if (!error) {
        this.response.statusCode = 200;
        this.response.end(result);
      } else {
        this.response.statusCode = error.error;
        this.response.end(error.reason);
      }
    });
  });

Router.route('/fuseki/personsWithAge', { where: 'server' })
  .get(function () {
    Meteor.call('fuseki.personsWithAge', (error, result) => {
      if (!error) {
        this.response.statusCode = 200;
        this.response.end(result);
      } else {
        this.response.statusCode = error.error;
        this.response.end(error.reason);
      }
    });
  });

Router.route('/fuseki/children/:personName', { where: 'server' })
  .get(function () {
    Meteor.call('fuseki.children', this.params.personName, (error, result) => {
      if (!error) {
        this.response.statusCode = 200;
        this.response.end(result);
      } else {
        this.response.statusCode = error.error;
        this.response.end(error.reason);
      }
    });
  });
