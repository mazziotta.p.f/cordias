
import '/imports/startup/fuseki.js';
import '/imports/startup/rdflib.js';

import '/imports/api/graphDb.class.js';

import '/imports/api/images/images.js';
import '/imports/api/images/images.publications.js';
import '/imports/api/images/images.methods.js';
import '/imports/api/shapes/shapes.methods.js';
import '/imports/api/concepts/concepts.methods.js';
import '/imports/api/properties/properties.methods.js';

import '/imports/ui/body.routes.js';
