
export class Export {

  DOMURL;

  canvas;
  backgroundId;

  exportCanvas;
  background;
  drawing;
  drawingUrl;
  context;
  fileName;

  // The canvas given here comes from SVG.js
  // backgroundId is optionnal
  constructor(canvas, backgroundId) {
    this.canvas = canvas;
    this.backgroundId = backgroundId;
    this.DOMURL = window.URL || window.webkitURL || window;
  }

  exportAsPNG(fileName, renderBackground) {
    this.fileName = fileName;
    this.drawing = new Image();
    this.drawingUrl = this.DOMURL.createObjectURL(this.drawingBlob);
    this.buildExportCanvas();

    this.drawing.onload = this.onLoadDrawing;

    if (this.backgroundId && renderBackground) {
      this.background = new Image();
      this.background.onload = this.onLoadBackground;
      this.loadBackground();
    }
    else this.loadDrawing();
  }

  loadBackground() {
    this.background.src = this.backgroundUrl;
  }

  loadDrawing() {
    this.drawing.src = this.drawingUrl;
  }

  onLoadBackground = () => {
    this.context.drawImage(this.background, 0, 0);
    this.loadDrawing();
  }

  onLoadDrawing = () => {
    this.context.drawImage(this.drawing, 0, 0);
    this.DOMURL.revokeObjectURL(this.drawingUrl);
    this.saveAs(this.exportCanvas.toDataURL('image/png'), this.fileName, 'png');
    this.cleanUp();
  }

  buildExportCanvas() {
    let canvas = document.createElement('canvas');
    canvas.width = this.canvas.width();
    canvas.height = this.canvas.height();
    this.context = canvas.getContext('2d');
    this.exportCanvas = canvas;
  }

  cleanUp() {
    this.exportCanvas && this.exportCanvas.constructor === HTMLCanvasElement ?
      this.exportCanvas.remove() : null;
    this.exportCanvas = null;
    this.background && this.background.constructor === HTMLImageElement ?
      this.background.remove() : null;
    this.background = null;
    this.drawing && this.drawing.constructor === HTMLImageElement ?
      this.drawing.remove() : null;
    this.drawing = null;
    this.drawingUrl = null;
    this.context = null;
    this.fileName = null;
  }

  get backgroundUrl() {
    return this.backgroundId ? document.getElementById(this.backgroundId).src : null;
  }

  get drawingCode() {
    return this.canvas.node.outerHTML;
  }

  get drawingBlob() {
    return new Blob([this.drawingCode], {type: 'image/svg+xml'});
  }

  saveAs(data, fileName, extension) {
    let link = document.createElement('a');
    link.download = fileName ? fileName : 'download' + '.' + extension;
    link.href = data;
    link.click();
  }
}
