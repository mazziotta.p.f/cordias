
import moment from 'moment';

export class Formatter {

  static formatDate(timestamp) {
    return moment.unix(timestamp/1000).format('YYYY/MM/DD HH:mm:ss:SS');
  }
}
