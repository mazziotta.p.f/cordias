
RDFLib = require('rdflib');

//FOAF = RDFLib.Namespace("http://xmlns.com/foaf/0.1/");
RDF = RDFLib.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
RDFS = RDFLib.Namespace("http://www.w3.org/2000/01/rdf-schema#");
OWL = RDFLib.Namespace("http://www.w3.org/2002/07/owl#");
DC = RDFLib.Namespace("http://purl.org/dc/elements/1.1/");
//RSS = RDFLib.Namespace("http://purl.org/rss/1.0/");
//XSD = RDFLib.Namespace("http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/#dt-");
//CONTACT = RDFLib.Namespace("http://www.w3.org/2000/10/swap/pim/contact#");

VIZ = RDFLib.Namespace("http://cordias/viz/");
PROP = RDFLib.Namespace("http://cordias/prop/");
CONCEPT = RDFLib.Namespace("http://cordias/concept/");
SHAPE = RDFLib.Namespace("http://cordias/shape/");
GRAPH = RDFLib.Namespace("http://cordias/graph/");
