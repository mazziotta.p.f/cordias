
BodyController = RouteController.extend({
  template: 'Home',
  layoutTemplate: 'DefaultLayout',
  yieldRegions: {
    'Files': {to: 'files'},
    'Editor': {to: 'editor'},
    'Properties': {to: 'properties'},
    'Concepts': {to: 'concepts'},
  },
});
