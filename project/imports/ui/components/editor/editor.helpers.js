
import { Formatter } from '/lib/formatter.class.js';

Template.EditorDrawing.helpers({
  toolIsActive: toolName => Editor.tools ? Editor.tools.isActive(toolName) : false,
  history: () => Editor.history.list(),
  formattedDate: timestamp => Formatter.formatDate(timestamp),
  isCurrentTimeStamp: timestamp => Router.current().params.timestamp == timestamp,
  routeHasTimestamp: () => Router.current().params.timestamp ? true: false,
});

Template.EditorToolButton.helpers({
  toolIsActive: toolName => Editor.tools ? Editor.tools.isActive(toolName) : false,
});
