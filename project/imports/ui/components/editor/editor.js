
import { DrawingEditor } from './classes/editor.class.js';

import './style/editor.css';

import './templates/editor.html';

import './editor.events.js';
import './editor.helpers.js';

Editor = new DrawingEditor();
