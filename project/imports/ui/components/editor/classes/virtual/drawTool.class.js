
import { Tool } from './tool.class.js';

export class DrawTool extends Tool {

  shapeBeingDrawn;
  canvas;
  activationLock;

  constructor(shapes, shapesDependency, canvas) {
    super(shapes, shapesDependency);
    this.canvas = canvas;
    this.activationLock = false;
  }

  use() {
    this.draw();
  }

  putAway() {
    if (this.shapeBeingDrawn) {
      const shape = this.shapeBeingDrawn;
      this.shapeBeingDrawn = null;
      // for some reason, drawing a rectangle with no height gives it a height of 1
      if (shape.width() && shape.height() > 1) {
        shape.draw('stop');
        shape.id(SHAPE('s_' + this.objectId()).value);
        this.addShape(shape);
        this.savingHandler();
      }
      else shape.remove();
    }
  }

  draw() {
    console.log('"draw()" method for draw tool "' + this.constructor.name + '" is not implemented');
  }

  addShape(shape) {
    this.shapes[shape.id()] = shape;
    this.shapesDependency.changed();
  }

  objectId() {
    var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
    return timestamp + 'xxxxxxxxxxxxxxxx'
      .replace(/[x]/g, () => (Math.random() * 16 | 0).toString(16)).toLowerCase();
  }
}
