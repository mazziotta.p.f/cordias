
export class Tool {

  _isActive;
  _isActiveDependency;
  isPersistent;
  shapes;
  shapesDependency;
  stateLock;

  constructor(shapes, shapesDependency) {
    this._isActive = false;
    this.isPersistent = false;
    this.shapes = shapes;
    this.shapesDependency = shapesDependency;
    this._isActiveDependency = new Tracker.Dependency();
    this.stateLock = false;
  }

  get isActive() {
    this._isActiveDependency.depend();
    return this._isActive;
  }

  set isActive(value) {
    this._isActive = value;
    this._isActiveDependency.changed();
  }

  activate() {
    // in case a toggle cycle is invalidating activation,
    // break the cycle.
    if (this.stateLock) return;
    this.isActive = true;
    this.use();
  }

  deactivate() {
    // in case a toggle cycle is invalidating deactivation,
    // lock the state of the tool for one second.
    this.stateLock = true;
    setTimeout( () => this.stateLock = false, 50);
    this.putAway();
    this.isActive = false;
  }

  toggle() {
    this.isActive ? this.deactivate() : this.activate();
  }

  use() {
    console.log('"use()" method for class "' + this.constructor.name + '" is not implemented');
  }

  putAway() {
    console.log('"putAway()" method for class "' + this.constructor.name + '" is not implemented');
  }

  savingHandler() {
    Editor.save(true);
  }
}
