
export class EditorGraph {

  _graph;
  shapes;
  canvas;
  graphName;
  shapesDependency;

  commonShapeAttributes = [
    'width', 'height',
    'stroke', 'stroke-width',
    'fill', 'fill-opacity',
    'x', 'y', 'x1', 'y1', 'x2', 'y2',
    'transform', 'points'
  ];

  constructor(shapes, canvas, graphName, shapesDependency) {
    this.graph = RDFLib.graph();
    this.shapes = shapes;
    this.canvas = canvas;
    this.graphName = graphName;
    this.shapesDependency = shapesDependency;
  }

  toString() {
    return this.graph.statements.join('');
  }

  add(shape) {
    const shapeElement = shape.node;
    if (!shapeElement.tagName) return;
    let blankNode = RDFLib.blankNode();
    if (shapeElement.id.startsWith(SHAPE().value))
      this.graph.add(blankNode, RDF('ID'), shapeElement.id);
    this.graph.add(blankNode, RDF('type'), VIZ('Shape'));
    this.graph.add(blankNode, VIZ('type'), shapeElement.tagName);
    this.commonShapeAttributes.forEach(attribute => {
      if (shapeElement.hasAttribute(attribute))
        this.graph.add(blankNode, VIZ(attribute), shapeElement.getAttribute(attribute));
    });
  }

  refresh() {
    this.graph = RDFLib.graph();
    Object.keys(this.shapes).forEach(key => {
      this.add(this.shapes[key]);
    });
  }

  parse(node) {
    let shape = {};
    shape.id = this.graph.any(node, RDF('ID')).value;
    shape.type = this.graph.any(node, VIZ('type')).value;
    this.commonShapeAttributes.forEach(attribute => {
      let result = this.graph.any(node, VIZ(attribute), undefined);
      if (!result || !result.value) return;
      shape[attribute] = result.value;
    });
    return shape;
  }

  load(timestamp) {
    Meteor.call('shapes.findCreatedAt', this.graphName, timestamp,
      (error, result) => {
        if (error) console.log(error);
        if (error || !result) return false;

        try {
          RDFLib.parse(result, this.graph, this.graphName, 'text/turtle');
          this.graph.each(undefined, RDF('type'), VIZ('Shape')).forEach(node => {
            this.inject(this.parse(node));
          });
          this.shapesDependency.changed();
        } catch (err) {
          console.log(err);
        }
      }
    );
  }

  inject(shapeAttributes) {
    if (!shapeAttributes.id || !shapeAttributes.type) return;
    let newShape = null;
    switch (shapeAttributes.type) {
      case 'rect': newShape = this.canvas.rect(); break;
      case 'polygon': newShape = this.canvas.polygon(shapeAttributes.points); break;
      case 'line': newShape = this.canvas.line(); break;
      default:
        console.log('Shape type is not recognized');
    }
    Object.keys(shapeAttributes).forEach(key => {
      newShape.node.setAttribute(key, shapeAttributes[key]);
    });
    newShape
      .attr('pointer-events', 'visible')
      .attr('data-drawing-action', 'resize-shape');
    this.shapes[newShape.id()] = newShape;
  }

  get graph() {
    return this._graph;
  }

  set graph(value) {
    this._graph = value;
  }
}
