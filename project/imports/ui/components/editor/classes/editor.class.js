
import SVG from 'svg.js';
import 'svg.draggable.js';
import 'svg.select.js';
import 'svg.resize.js';
import 'svg.draw.js';
import moment from 'moment';

import '/node_modules/svg.select.js/dist/svg.select.css';

import { EditorTools } from './editorTools.class.js';
import { EditorGraph } from './editorGraph.class.js';
import { Formatter } from '/lib/formatter.class.js';
import { Export } from '/lib/export.class.js';

export class DrawingEditor {

  isLoading;

  _canvas;
  _graphName;
  _history;
  _tools;
  _toolsDependency;
  _graph;
  _shapes;
  _shapesDependency;

  constructor() {
    this.isLoading = false;
    this._shapes = {};
    this._graphName = null;
    this._graph = null;
    this._tools = null;
    this._history = new ReactiveArray();
    this._toolsDependency = new Tracker.Dependency();
    this._shapesDependency = new Tracker.Dependency();
  }

  load(timestamp) {
    if (this.isLoading) return;
    const imageData = Router.current().data().image();
    if (imageData && imageData._id) {
      this.isLoading = true;
      const image = new Image();
      let _this = this;
      image.onload = function() {
        try {
          _this.reset('drawing', this.width, this.height,
            GRAPH('g_' + imageData._id).value, timestamp);
        } catch (exception) {
          console.log(exception);
        } finally {
          _this.isLoading = false;
        }
      };
      image.src = imageData.link();
    }
  }

  save(doNotRedirect) {
    //this.tools.deactivateAll();
    this.graph.refresh();
    const graphData = this.graph.toString();
    const imageId = Router.current().params.imageId;
    if (!graphData) return;
    Meteor.call('shapes.addData', this.graphName, graphData,
      (error, result) => {
        if (!error && result){
          const timestamp = result.toString();
          if (doNotRedirect) {
            Router.current().params.timestamp = timestamp;
            const pageTitle = 'CORDIAS ~ ' + Router.current().data().image().name
                + ' > ' + Formatter.formatDate(Router.current().params.timestamp);
            document.title = pageTitle;
            history.pushState({}, pageTitle, Router.path('editGraphCreatedAt', {imageId, timestamp}))
            this.history.unshift(timestamp);
          }
          else
            Router.go('editGraphCreatedAt', { imageId, timestamp });
        }
      }
    );
  }

  exportImage() {
    let exporter = new Export(this.canvas, 'drawing-background');
    let fileName = 'export_' + Router.current().params.imageId
      + '_' + moment().format('YYYY-MM-DD_HH-mm-ss');
    exporter.exportAsPNG(fileName, true);
  }

  reset(elementId, width, height, graphName, timestamp) {
    this.clear();
    if (!elementId) elementId = 'drawing';
    if (!width) width = '100%';
    if (!height) height = '100%';
    this.canvas = this.buildCanvas(elementId, width, height);
    this.tools = new EditorTools(this.shapes, this.canvas, this._shapesDependency);
    if (graphName) {
      this.graphName = graphName;
      this.graph = new EditorGraph(this.shapes, this.canvas, this.graphName, this._shapesDependency);
    }
    this.loadHistory(timestamp);
  }

  loadHistory(timestamp) {
    Meteor.call('shapes.fetchHistory', this.graphName, (error, result) => {
      if (error) {
        console.log(error);
        if (!result) return false;
      }

      this.history.clear();
      result.forEach(entry => {
        this.history.push(entry.timestamp.value);
      });
      if (timestamp) {
        this.graph.load(timestamp);
      }
    });
  }

  next() {
    if (!this.history.length) return;
    this.tools.deactivateAll();
    if (Router.current().route.getName() === 'editGraph')
    {
      Router.go('editGraphCreatedAt', {
        imageId: Router.current().params.imageId,
        timestamp: this.history[this.history.length -1]
      });
      return;
    }
    const index = this.historyIndex;
    if (index === null || index <= 0) return;

    Router.go('editGraphCreatedAt', {
      imageId: Router.current().params.imageId,
      timestamp: this.history[index -1]
    });
  }

  previous() {
    this.tools.deactivateAll();
    const index = this.historyIndex;
    if (index === null || index > this.history.length -1) return;

    if (index == this.history.length -1)
      Router.go('editGraph', {
        imageId: Router.current().params.imageId,
      });
    else
      Router.go('editGraphCreatedAt', {
        imageId: Router.current().params.imageId,
        timestamp: this.history[index +1]
      });
  }

  new() {
    this.tools.deactivateAll();
    Router.go('editGraph', {
      imageId: Router.current().params.imageId,
    });
  }

  latest() {
    this.tools.deactivateAll();
    Router.go('editGraphCreatedAt', {
      imageId: Router.current().params.imageId,
      timestamp: 'latest'
    });
  }

  get historyIndex() {
    const timestamp = Router.current().params.timestamp;
    if (!timestamp) return null;
    const index = this.history.indexOf(timestamp);
    return index;
  }

  clear() {
    if (this.shapes) Object.values(this.shapes)
      .forEach((shape) => {shape.remove()});
    if (this.canvas) this.canvas = null;
    this.history.clear();
    this.shapes = {};
    this.graphName = null;
    this.graph = null;
    this.tools = null;
    if (this.tools) this.tools.deactivateAll();
  }

  buildCanvas(elementId, width, height) {
    if (!document.getElementById(elementId)) return;
    return SVG(elementId).size(width, height);
  }

  get graphName() {
    return this._graphName;
  }

  set graphName(value) {
    this._graphName = value;
  }

  get canvas() {
    return this._canvas;
  }

  set canvas(value) {
    if (this.canvas) this.canvas.remove();
    this._canvas = value;
  }

  get history() {
    return this._history;
  }

  set history(value) {
    this._history = value;
  }

  get tools() {
    this._toolsDependency.depend();
    return this._tools;
  }

  set tools(value) {
    this._tools = value;
    this._toolsDependency.changed();
  }

  get graph() {
    return this._graph;
  }

  set graph(value) {
    this._graph = value;
  }

  get shapes() {
    this._shapesDependency.depend();
    return this._shapes;
  }

  set shapes(value) {
    this._shapes = value;
    this._shapesDependency.changed();
  }
}
