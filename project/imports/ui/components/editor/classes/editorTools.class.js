
import { DragTool } from './tools/dragTool.class.js';
import { LineTool } from './tools/lineTool.class.js';
import { PolygonTool } from './tools/polygonTool.class.js';
import { RectangleTool } from './tools/rectangleTool.class.js';
import { RemoveTool } from './tools/removeTool.class.js';
import { ResizeTool } from './tools/resizeTool.class.js';
import { IdentityTool } from './tools/identifyTool.class.js';
import { HistoryTool } from './tools/historyTool.class.js';

export class EditorTools {

  tools;
  shapes;
  canvas;

  constructor(shapes, canvas, shapesDependency) {
    this.shapes = shapes;
    this.canvas = canvas;
    this.tools = {
      drag: new DragTool(this.shapes, shapesDependency),
      resize: new ResizeTool(this.shapes, shapesDependency),
      rectangle: new RectangleTool(this.shapes, shapesDependency, this.canvas),
      polygon: new PolygonTool(this.shapes, shapesDependency, this.canvas),
      line: new LineTool(this.shapes, shapesDependency, this.canvas),
      remove: new RemoveTool(this.shapes, shapesDependency),
      identify: new IdentityTool(this.shapes, shapesDependency, this.canvas),
      history: new HistoryTool(),
    };
  }

  isActive(toolName) {
    return this.tools[toolName].isActive;
  }

  isPersistent(toolName) {
    return this.tools[toolName].isPersistent;
  }

  toggle(toolName) {
    if (!this.tools[toolName].isActive) this.deactivateAll(toolName);
    this.tools[toolName].toggle();
  }

  activate(toolName) {
    if (this.tools[toolName].isActive) return;
    this.deactivateAll(toolName);
    this.tools[toolName].activate();
  }

  deactivate(toolName) {
    this.tools[toolName].deactivate();
  }

  deactivateAll(exceptToolName) {
    Object.keys(this.tools).forEach(toolName => {
      if (exceptToolName) {
        if (toolName !== exceptToolName) this.deactivate(toolName);
      }
      else if (!this.isPersistent(toolName) && toolName !== exceptToolName)
        this.deactivate(toolName);
    });
  }

  removeShape(shapeId) {
    this.tools['remove'].removeShape(shapeId);
  }

  resizeShape(shapeId) {
    this.tools['resize'].resizeShape(shapeId);
  }

  selectShape(shapeId) {
    this.activate('resize');
    this.tools['resize'].selectShape(shapeId);
  }

  highlightShape(shapeId) {
    this.tools['identify'].highlightShape(shapeId);
  }

  get selectedShape() {
    return this.tools['resize'].selectedShape;
  }
}
