
import { DrawTool } from '../virtual/drawTool.class.js';

export class LineTool extends DrawTool {

  draw(strokeColor) {
    let line = this.canvas.line().draw();
    let opacity = 0.2;
    let width = 3;

    this.shapeBeingDrawn = line;

    if (strokeColor) line.stroke({ color: strokeColor, width });
    else line.stroke({ color: 'black', width });

    line
      .attr('pointer-events', 'visible')
      .attr('data-drawing-action', 'resize-shape');

    let mousedownListener = (event) => {
      line.draw(event);
    };

    let mouseupListener = (event) => {
      line.draw('stop', event);
      this.deactivate();
    };

    document.addEventListener('mousedown', mousedownListener);
    document.addEventListener('mouseup', mouseupListener);

    line.on('drawstop', () => {
      document.removeEventListener('mousedown', mousedownListener);
      document.removeEventListener('mouseup', mouseupListener);
    });
  }
}
