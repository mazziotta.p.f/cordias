
import { Tool } from '../virtual/tool.class.js';

export class DragTool extends Tool {

  constructor(editorShapes, shapesDependency) {
    super(editorShapes, shapesDependency);
    this.isPersistent = true;
  }

  use() {
    if (!this.shapes) return;
    Object.values(this.shapes).forEach(shape => shape
      .draggable()
      .on('dragend', this.savingHandler)
    );
  }

  putAway() {
    if (!this.shapes) return;
    Object.values(this.shapes).forEach(
      shape => shape.draggable(false).off('dragend')
    );
  }
}
