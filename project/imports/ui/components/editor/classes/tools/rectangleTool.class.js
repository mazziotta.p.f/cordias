
import { DrawTool } from '../virtual/drawTool.class.js';

export class RectangleTool extends DrawTool {

  draw(strokeColor, fillColor) {
    let rectangle = this.canvas.rect().draw();
    let opacity = 0.2;
    let width = 3;

    this.shapeBeingDrawn = rectangle;

    if (strokeColor) rectangle.stroke({ color: strokeColor, width });
    else rectangle.stroke({ color: 'black', width });
    if (fillColor) rectangle.fill({ color: fillColor, opacity });
    else rectangle.fill({ color: 'black', opacity });

    rectangle
      .attr('pointer-events', 'visible')
      .attr('data-drawing-action', 'resize-shape');

    let mousedownListener = (event) => {
      rectangle.draw(event);
    };

    let mouseupListener = (event) => {
      rectangle.draw('stop', event);
      this.deactivate();
    };

    document.addEventListener('mousedown', mousedownListener);
    document.addEventListener('mouseup', mouseupListener);

    rectangle.on('drawstop', () => {
      document.removeEventListener('mousedown', mousedownListener);
      document.removeEventListener('mouseup', mouseupListener);
    });
  }
}
