
import { Tool } from '../virtual/tool.class.js';

export class HistoryTool extends Tool {

  historyElement;

  constructor() {
    super();
    this.historyElement = document.getElementById('history');
  }

  use() {
    this.historyElement.style.display = 'inline';
  }

  putAway() {
    this.historyElement.style.display = 'none';
  }
}
