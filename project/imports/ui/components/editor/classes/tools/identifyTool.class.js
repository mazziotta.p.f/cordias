
import { Tool } from '../virtual/tool.class.js';

export class IdentityTool extends Tool {

  labels;
  canvas;
  highlight;

  constructor(editorShapes, shapesDependency, canvas) {
    super(editorShapes, shapesDependency);
    this.labels = [];
    this.canvas = canvas;
  }

  use() {
    let index = 1;
    Object.keys(this.shapes)
      .forEach(key => this.addNumbering(this.shapes[key], index++));
  }

  putAway() {
    this.labels.forEach(shape => shape.remove());
  }

  addNumbering(shape, index) {
    let rect = this.canvas
      .rect()
      .radius(2)
      .move(shape.x(), shape.y());
    let text = this.canvas
      .text(index.toString())
      .font({size: 10, fill: '#FFFFFF'})
      .move(shape.x() +2, shape.y() +1);
    let box = text.rbox();
    rect.width(box.w +5);
    rect.height(box.h +2);
    this.labels.push(rect);
    this.labels.push(text);
  }

  highlightShape(shapeId) {
    if (this.highlight) this.highlight.remove();
    const shape = this.shapes[shapeId];
    let rect = this.canvas
      .rect(shape.width() +8, shape.height() +8)
      .stroke({ color: 'red', width: 2 })
      .fill({ opacity: 0 })
      .move(shape.x() -4, shape.y() -4);
    this.labels.push(rect);
    this.highlight = rect;
  }
}
