
import { DrawTool } from '../virtual/drawTool.class.js';

export class PolygonTool extends DrawTool {

  draw(strokeColor, fillColor) {
    let polygon = this.canvas.polygon().draw();
    let opacity = 0.2;
    let width = 3;

    this.shapeBeingDrawn = polygon;

    if (strokeColor) polygon.stroke({ color: strokeColor, width });
    else polygon.stroke({ color: 'black', width });
    if (fillColor) polygon.fill({ color: fillColor, opacity });
    else polygon.fill({ color: 'black', opacity });

    polygon
      .attr('pointer-events', 'visible')
      .attr('data-drawing-action', 'resize-shape');

    let listener = (event) => {
      if(event.keyCode == 13){
        polygon.draw('done');
        polygon.off('drawstart');
        this.deactivate();
      }
    };

    polygon.on('drawstart', () => {
      document.addEventListener('keypress', listener);
    });

    polygon.on('drawstop', () => {
      document.removeEventListener('keypress', listener);
    });
  }
}
