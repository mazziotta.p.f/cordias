
import { Tool } from '../virtual/tool.class.js';

export class RemoveTool extends Tool {

  constructor(editorShapes, shapesDependency) {
    super(editorShapes, shapesDependency);
    this.isPersistent = true;
  }

  use() {
    if (!this.shapes) return;
    Object.keys(this.shapes)
      .forEach(key => this.shapes[key].attr('data-remove-action', 'true'));
  }

  putAway() {
    if (!this.shapes) return;
    Object.keys(this.shapes)
      .forEach(key => this.shapes[key].attr('data-remove-action', 'false'));
  }

  removeShape(shapeId) {
    if (!this.shapes) return;
    this.shapes[shapeId].remove();
    delete this.shapes[shapeId];
    this.shapesDependency.changed();
    this.savingHandler();
  }
}
