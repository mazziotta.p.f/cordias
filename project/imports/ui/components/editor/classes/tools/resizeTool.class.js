
import { Tool } from '../virtual/tool.class.js';

export class ResizeTool extends Tool {

  _selectedShape;
  _selectedShapeDependency;

  constructor(editorShapes, shapesDependency) {
    super(editorShapes, shapesDependency);
    this._selectedShape = null;
    this._selectedShapeDependency = new Tracker.Dependency();
    this.isPersistent = true;
  }

  get selectedShape() {
    this._selectedShapeDependency.depend();
    return this._selectedShape;
  }

  set selectedShape(value) {
    this._selectedShape = value;
    this._selectedShapeDependency.changed();
  }

  use() {
    return;
  }

  putAway() {
    this.unselectShape();
  }

  selectShape(shapeId) {
    this.resizeShape(shapeId);
  }

  resizeShape(shapeId) {
    if (!this.isActive) return;
    this.unselectShape();
    let shape = this.shapes[shapeId];
    if (shape) {
      this.selectedShape = shape;
      shape
        .selectize()
        .selectize({deepSelect:true})
        .resize()
        .draggable()
        .on('resizedone', this.savingHandler)
        .on('dragend', this.savingHandler);
    }
  }

  unselectShape() {
    if (!this.selectedShape) return;
    if (this.selectedShape.type === 'rect')
      this.selectedShape
        .selectize(false);
    else
      this.selectedShape
        .selectize(false)
        .selectize(false, {deepSelect:true});
    this.selectedShape
      .resize('stop')
      .draggable(false)
      .off('resizedone')
      .off('dragend');
    this.selectedShape = null;
  }
}
