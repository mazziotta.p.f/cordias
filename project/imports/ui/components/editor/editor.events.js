
Template.EditorDrawing.events({
  'change [data-action="navigate-to"]'(event, template) {
    Router.go('editGraphCreatedAt', {
      imageId: Router.current().params.imageId,
      timestamp: event.target.value
    });
  },
  'mousedown [data-drawing-action="resize-shape"]'(event, template) {
    Editor.tools.resizeShape(event.target.id);
  },
  'click [data-remove-action="true"]'(event, template) {
    Editor.tools.removeShape(event.target.id);
  },
});

Template.EditorToolButton.events({
  'click [data-drawing-action="toggle-tool"]'(event, template) {
    Editor.tools.toggle(event.target.dataset.tool);
  },
});

Template.EditorActionButton.events({
  'click [data-action="execute"]'(event, template) {
    Editor[event.target.dataset.operation]();
  },
});
