
Template.Concepts.helpers({
  selectedConcept: () => Concepts.selectedConcept,
  displayCreateForm: () => Concepts.displayCreateForm,
});

Template.ConceptEditForm.helpers({
  selectedConcept: () => Concepts.selectedConcept,
});

Template.ConceptsList.helpers({
  concepts: () => Concepts.all,
});
