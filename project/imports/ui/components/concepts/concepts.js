
import './style/concepts.css';

import './templates/concepts.html';

import './concepts.events.js';
import './concepts.helpers.js';

import { ConceptsManager } from './classes/concepts.class.js';

Concepts = new ConceptsManager();
