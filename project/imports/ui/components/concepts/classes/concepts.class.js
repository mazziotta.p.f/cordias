
export class ConceptsManager {

    _concepts;
    _conceptsDependency;
    _selectedConcept;
    _selectedConceptDependency;
    _displayCreateForm;
    _displayCreateFormDependency;

  constructor() {
    this._concepts = [];
    this._conceptsDependency = new Tracker.Dependency();
    this._selectedConcept = null;
    this._selectedConceptDependency = new Tracker.Dependency();
    this._displayCreateForm = false;
    this._displayCreateFormDependency = new Tracker.Dependency();
    this.load();
  }

  load() {
    let graph = RDFLib.graph();
    this.concepts = [];
    this.selectedConcept = null;

    try {
      Meteor.call('concepts.fetchAll', (error, result) => {
        RDFLib.parse(result, graph, CONCEPT('graph').value, 'text/turtle');

        graph.each(undefined, RDF('type'), CONCEPT('instance')).forEach(node => {
          let name = graph.any(node, DC('name'), undefined);
          let color = graph.any(node, DC('color'), undefined);
          let description = graph.any(node, DC('description'), undefined);
          this.concepts.push({
            _id: node.value,
            name: name ? name.value : '',
            color: color ? color.value : '',
            description: description ? description.value : '',
          });
        });
        this.sort();
        this._conceptsDependency.changed();
      });
    } catch (err) {
      console.log(err);
    }
  }

  toggleCreateForm() {
    this.displayCreateForm = !this.displayCreateForm;
  }

  add(name, color, description) {
    Meteor.call('concepts.create', name, color, description,
      (error, result) => {
        if (error) {
          console.log(error);
          return;
        }

        this.concepts.push({ _id: result, name, color, description });
        this.sort();
        this._conceptsDependency.changed();
        this.toggleCreateForm();
      }
    );
  }

  update(conceptId, name, color, description) {
    const concept = this.find(conceptId);
    if (!concept) return;
    const index = this.concepts.indexOf(concept);
    if (index < 0) return;
    try {
      Meteor.call('concepts.update', conceptId, name, color, description, (error, result) => {
        if (error) {
          console.log(error);
          return;
        }
        if (!result) return;
        this.concepts.splice(index, 1, {
          _id: conceptId, name, color, description
        });
        this.unselect();
        this._conceptsDependency.changed();
      });
    } catch (err) {
      console.log(err);
    }
  }

  remove(conceptId) {
    const concept = this.find(conceptId);
    if (!concept) return;
    const index = this.concepts.indexOf(concept);
    if (index < 0) return;
    try {
      Meteor.call('concepts.remove', conceptId, (error, result) => {
        if (error) {
          console.log(error);
          return;
        }
        if (!result) return;
        this.concepts.splice(index, 1);
        this.unselect();
        this._conceptsDependency.changed();
      });
    } catch (err) {
      console.log(err);
    }
  }

  sort() {
    this.concepts.sort(function(a, b){
      if(a.name < b.name) return -1;
      if(a.name > b.name) return 1;
      return 0;
    });
  }

  find(conceptId) {
    return this.concepts.find(concept => concept._id === conceptId);
  }

  select(conceptId) {
    this.selectedConcept = this.find(conceptId);
    if (this.selectedConcept && this.displayCreateForm)
      this.toggleCreateForm();
  }

  unselect() {
    this.selectedConcept = null;
  }

  get concepts() {
    this._conceptsDependency.depend();
    return this._concepts;
  }

  set concepts(value) {
    this._concepts = value;
    this._conceptsDependency.changed();
  }

  get all() {
    return this.concepts;
  }

  set all(value) {
    this.concepts = value;
  }

  get selectedConcept() {
    this._selectedConceptDependency.depend();
    return this._selectedConcept;
  }

  set selectedConcept(value) {
    this._selectedConcept = value;
    this._selectedConceptDependency.changed();
  }

  get displayCreateForm() {
    this._displayCreateFormDependency.depend();
    return this._displayCreateForm;
  }

  set displayCreateForm(value) {
    this._displayCreateForm = value;
    this._displayCreateFormDependency.changed();
  }
}
