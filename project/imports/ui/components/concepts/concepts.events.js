
Template.Concepts.events({
  'click [data-view-action="toggle-create"]'(event, template) {
    Concepts.toggleCreateForm();
  },
});

Template.ConceptCreateForm.events({
  'submit [data-action="create-concept"]'(event, template) {
    event.preventDefault();
    const target = event.target;

    if(!target.name.value) return;

    Concepts.add(target.name.value, target.color.value, target.description.value);

    target.name.value = '';
    target.color.value = '';
    target.description.value = '';
  },
  'click [data-view-action="toggle"]'(event, template) {
    Concepts.toggleCreateForm();
  },
});

Template.ConceptsListEntry.events({
  'click [data-view-action="select"]'(event, template) {
    Concepts.select(this._id);
  },
});

Template.ConceptEditForm.events({
  'submit [data-action="edit-concept"]'(event, template) {
    event.preventDefault();
    const target = event.target;

    if(!target.name.value) return;

    Concepts.update(this._id, target.name.value, target.color.value, target.description.value);
  },
  'click [data-action="remove"]'(event, template) {
    if(confirm('Are you sure you want to remove this concept ?')) Concepts.remove(this._id);
  },
  'click [data-view-action="unselect"]'(event, template) {
    Concepts.unselect();
  },
});
