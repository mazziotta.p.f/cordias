
import { Images } from '/imports/api/images/images.js';

Template.Files.events({
  'change [data-action="upload-file"]'(event, template) {

    const files = event.target.files;

    if (files && files[0]) {
      const upload = Images.insert({
        file: files[0],
        streams: 'dynamic',
        chunkSize: 'dynamic'
      }, false);

      upload.start();
    }

    event.target.value = "";
  },
});

Template.FileLine.events({
  'click [data-action="remove"]'(event, template) {
    if (confirm('Remove this file ?')) {
      Meteor.call('images.remove', this._id);
      Router.go('home');
    }
  },
});
