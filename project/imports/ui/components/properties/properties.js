
import './style/properties.css';
import './templates/properties.html';

import './properties.helpers.js';
import './properties.events.js';

import { AnnotationProperties } from './classes/properties.class.js';

Properties = new AnnotationProperties();
