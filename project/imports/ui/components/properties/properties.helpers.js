
Template.Properties.helpers({
  shapeIsSelected: () => Properties.shapeIsSelected(),
  selectedShape: () => Properties.selectedShape,
  shapes: () => Properties.shapes,
});

Template.PropertiesConceptSelect.helpers({
  conceptIsSelected: () => Properties.selectedShape.concept,
  concepts: () => Concepts.all,
  optionIsSelected: () => Template.currentData()._id === Properties.selectedShape.concept,
});
