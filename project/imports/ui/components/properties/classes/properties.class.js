
import { PropertiesGraph } from './propertiesGraph.class.js';
import { ShapeProperties } from './shapeProperties.class.js';

export class AnnotationProperties {

  graph;
  _selectedShape;
  _selectedShapeDependency;

  constructor() {
    this.load();
  }

  load() {
    this.graph = new PropertiesGraph();
    this._selectedShape = null;
    this._selectedShapeDependency = new Tracker.Dependency();
  }

  save() {
    Meteor.call('properties.add', this.graph.toString());
  }

  assignConcept(conceptId) {
    if (!this.selectedShape) {
      console.log('Could not assign concept : no shape is selected');
      return false;
    }
    if (!this.selectedShape.id || !this.selectedShape.id.startsWith(SHAPE('s_').value)) {
      console.log('Could not assign concept : no shape id or malformed shape id');
      return false;
    }
    const graphName = GRAPH('g_' + Router.current().params.imageId).value;
    Meteor.call('properties.setConcept', graphName, this.selectedShape.id, conceptId);
    let concept = Concepts.find(conceptId);
    if (concept.color && concept.color.length === 3 || concept.color.length === 6)
      this.selectedShape.shape.attr({fill: '#' + concept.color, stroke: '#' + concept.color});
    else
      this.selectedShape.shape.attr({fill: 'black', stroke: 'black'});
    Editor.save(true);
  }

  selectShape(shapeId) {
    Editor.tools.activate('identify');
    this.selectedShape = new ShapeProperties(Editor.shapes[shapeId]);
    const graphName = GRAPH('g_' + Router.current().params.imageId).value;
    Meteor.call('properties.getConcept', graphName, this.selectedShape.id, (error, result) => {
      this.selectedShape.concept = result;
      this._selectedShapeDependency.changed();
      Editor.tools.highlightShape(shapeId);
    });
  }

  get selectedShape() {
    this._selectedShapeDependency.depend();
    return this._selectedShape;
  }

  set selectedShape(value) {
    this._selectedShape = value;
    this._selectedShapeDependency.changed();
  }

  get shapes() {
    let index = 1;
    return Object.keys(Editor.shapes).map( key => new ShapeProperties(Editor.shapes[key], index++) );
  }

  shapeIsSelected() {
    return this.selectedShape && this.selectedShape.id.startsWith(SHAPE('s_').value) ? true : false;
  }
}
