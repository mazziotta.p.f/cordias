
export class PropertiesGraph {

  _graph;

  constructor() {
    this.graph = RDFLib.graph();
  }

  toString() {
    return this.graph.statements.join('');
  }

  get graph() {
    return this._graph;
  }

  set graph(value) {
    this._graph = value;
  }
}
