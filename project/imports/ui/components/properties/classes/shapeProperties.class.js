
export class ShapeProperties {

  _shape;
  _concept;
  _index;

  constructor(shape, index) {
    this._shape = shape;
    this._index = index;
  }

  get id() {
    return this.shape.id();
  }

  get name() {
    return this.shape.node.nodeName;
  }

  get shape() {
    return this._shape;
  }

  set shape(value) {
    this._shape = value;
  }

  get concept() {
    return this._concept;
  }

  set concept(value) {
    this._concept = value;
  }

  get index() {
    return this._index;
  }

  set index(value) {
    this._index = value;
  }

}
