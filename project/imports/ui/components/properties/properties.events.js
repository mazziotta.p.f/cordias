
Template.Properties.events({
  'click [data-drawing-action="select-shape"]'(event, template) {
    Properties.selectShape(this.id);
  },
});

Template.PropertiesConceptSelect.events({
  'change [data-action="set-concept"]'(event, template) {
    Properties.assignConcept(event.target.value);
  },
});
