
import { Images } from '/imports/api/images/images.js';
import { Formatter } from '/lib/formatter.class.js';

Router.route('/', {
  name: 'home',
  controller: 'BodyController',
  subscriptions: function() {
    Meteor.subscribe('images');
  },
  data: {
    images: () => Images.find(),
  },
});

Router.route('/:imageId', {
  name: 'editGraph',
  controller: 'BodyController',
  subscriptions: function() {
    Meteor.subscribe('images');
  },
  data: {
    images: () => Images.find(),
    image: () => Images.findOne({_id: Router.current().params.imageId}),
  },
  onAfterAction: () => {
    if (Router.current().data().image())
      document.title = 'CORDIAS ~ ' + Router.current().data().image().name;
    Editor.load();
    Properties.load();
  },
});

Router.route('/:imageId/:timestamp', {
  name: 'editGraphCreatedAt',
  controller: 'BodyController',
  subscriptions: function() {
    Meteor.subscribe('images');
  },
  data: {
    images: () => Images.find(),
    image: () => Images.findOne({_id: Router.current().params.imageId}),
  },
  onBeforeAction: function() {
    const timestamp = Router.current().params.timestamp;
    if (timestamp === 'latest') {
      const imageId = Router.current().params.imageId;
      const graphName = 'http://cordias/graph/g_' + imageId;
      Meteor.call('shapes.findLatestTimestamp', graphName,
        (error, result) => result ?
          Router.go('editGraphCreatedAt', {imageId, timestamp: result}, {replaceState: true}) :
          Router.go('editGraph', {imageId}, {replaceState: true})
      );
    }
    this.next();
  },
  onAfterAction: () => {
    if (Router.current().params.timestamp === 'latest') return;
    if (Router.current().data().image())
      document.title = 'CORDIAS ~ ' + Router.current().data().image().name
        + ' > ' + Formatter.formatDate(Router.current().params.timestamp);
    Editor.load(new Date(parseInt(Router.current().params.timestamp))),
    Properties.load();
  }
});
