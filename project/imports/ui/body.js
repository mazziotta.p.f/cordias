
import '/imports/startup/rdflib.js';

import './components/files/files.js';
import './components/editor/editor.js';
import './components/properties/properties.js';
import './components/concepts/concepts.js';

import './layout/layout.js';

import './body.controller.js';
import './body.routes.js';
