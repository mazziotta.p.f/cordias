
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { PropertiesGraphs } from '/imports/api/properties/properties.class.js';

Properties = new PropertiesGraphs(FUSEKI_DATASET, FUSEKI_URL);

Meteor.methods({
  /*
  'properties.find'(graphName, propertyId) {
    check(graphName, String);
    check(propertyId, String);

    return Properties.find(graphName, propertyId);
  },
  */
  'properties.addData'(graphName, graphData) {
    check(graphName, String);
    check(graphData, String);

    return Properties.add(graphName, graphData);
  },
  'properties.setConcept'(graphName, shapeId, conceptId) {
    check(graphName, String);
    check(shapeId, String);
    check(conceptId, String);

    return Properties.setConcept(graphName, shapeId, conceptId);
  },
  'properties.getConcept'(graphName, shapeId) {
    check(graphName, String);
    check(shapeId, String);

    return Properties.getConcept(graphName, shapeId);
  },
});
