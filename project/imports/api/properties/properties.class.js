
import { GraphDatabase } from '/imports/api/graphDb.class.js';

export class PropertiesGraphs extends GraphDatabase {

  add(graphName, graphData) {
    return this.addDataToGraph(graphName, graphData) ? timestamp : null;
  }

  setConcept(graphName, shapeId, conceptId) {
    const id = 'p_' + this.objectId();
    this.removeConcept(graphName, shapeId);
    // TODO : prevent injections
    let query =
      'PREFIX prop: <' + PROP().value + '> ' +
      'PREFIX shape: <' + SHAPE().value + '> ' +
      'PREFIX concept: <' + CONCEPT().value + '> ' +
      'INSERT DATA ' +
      '{ GRAPH <' + graphName + '> ' +
      '  { ' +
      '    prop:' + id + ' a prop:fact ;' +
      '      shape:id <' + shapeId + '> ;' +
      '      concept:id <' + conceptId + '> .' +
      '  }' +
      '}';

    this.query(query, 'update');
    return id;
  }

  getConcept(graphName, shapeId) {
    let query =
      'PREFIX prop: <' + PROP().value + '> ' +
      'PREFIX shape: <' + SHAPE().value + '> ' +
      'PREFIX concept: <' + CONCEPT().value + '> ' +
      'SELECT DISTINCT ?conceptId ' +
      'FROM <' + graphName + '> ' +
      'WHERE { ' +
      '	 ?property a prop:fact . ' +
      '  ?property shape:id <' + shapeId + '> . ' +
      '  ?property concept:id ?conceptId . ' +
      '} ' +
      'LIMIT 1';

    const result = this.query(query);

    if (result.length) return result[0].conceptId.value;
    return null;
  }

  removeConcept(graphName, shapeId) {
    let query =
      'PREFIX prop: <' + PROP().value + '> ' +
      'PREFIX shape: <' + SHAPE().value + '> ' +
      'WITH <' + graphName + '> ' +
      'DELETE { ?s ?p ?o }' +
      'WHERE ' +
      '{ ' +
      '  ?s a prop:fact .' +
      '  ?s shape:id <' + shapeId + '> . ' +
      '  ?s ?p ?o .' +
      '}';

    return this.query(query, 'update');
  }
}
