
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { ConceptsGraphs } from '/imports/api/concepts/concepts.class.js';

Concepts = new ConceptsGraphs(FUSEKI_DATASET, FUSEKI_URL);

Meteor.methods({
  'concepts.create'(name, color, description) {
    check(name, String);
    check(color, String);
    check(description, String);

    return Concepts.add(name, color, description);
  },
  'concepts.fetchAll'() {
    return Concepts.getAll();
  },
  'concepts.update'(conceptId, name, color, description) {
    check(conceptId, String);
    check(name, String);
    check(color, String);
    check(description, String);

    return Concepts.update(conceptId, name, color, description);
  },
  'concepts.remove'(conceptId) {
    check(conceptId, String);

    return Concepts.remove(conceptId);
  },
});
