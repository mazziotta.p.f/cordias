
import { GraphDatabase } from '/imports/api/graphDb.class.js';

export class ConceptsGraphs extends GraphDatabase {

  add(name, color, description, id) {
    if (!id) id = CONCEPT('c_' + this.objectId()).value;
    // TODO : prevent injections
    let query =
      'PREFIX concept: <' + CONCEPT().value + '> ' +
      'PREFIX dc: <' + DC().value + '> ' +
      'INSERT DATA ' +
      '{ ' +
      '  <' + id + '> a concept:instance ;' +
      '    dc:name "' + name + '" ;' +
      '    dc:color "' + color + '" ;' +
      '    dc:description "' + description.replace('\n', ' ') + '" .' +
      '}';

    this.query(query, 'update');
    return id;
  }

  getAll() {
    let query =
      'PREFIX concept: <' + CONCEPT().value + '> ' +
      'CONSTRUCT { ?s ?p ?o } ' +
      'WHERE { ' +
      '	 ?s a concept:instance . ' +
      '  ?s ?p ?o . ' +
      '}';

    return this.construct(query);
  }

  update(conceptId, name, color, description) {
    if (!this.remove(conceptId)) return false;
    return this.add(name, color, description, conceptId);
  }

  remove(conceptId) {
    let query =
      'PREFIX concept: <' + CONCEPT().value + '> ' +
      'DELETE ' +
      'WHERE { ' +
      '  <' + conceptId + '> ?p ?o . ' +
      '}';

    return this.query(query, 'update');
  }
}
