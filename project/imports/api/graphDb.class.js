
export class GraphDatabase {

  _dataset;
  _apiPath;

  constructor(dataset, apiPath) {
    this.dataset = dataset;
    this.apiPath = apiPath;
  }

  prepareParameter(parameter) {
    // TODO : prevent injections
    switch (parameter.type) {
      case 'uri':
        return '<' + parameter.value + '>';
        break;
      case 'literal':
        return '"' + parameter.value + '"';
        break;
      default:
        throw new Meteor.Error(500, 'Error 500: unrecognized parameter')
    }
  }

  remove(subject, predicate, object) {
    let query =
      'DELETE DATA ' +
      '{ ' +
      '  ' + this.prepareParameter(subject) + ' '
           + this.prepareParameter(predicate) + ' '
           + this.prepareParameter(object) + ' .' +
      '}';

    return this.query(query, 'update');
  }

  createGraph(uri) {
    let query = 'CREATE GRAPH <' + uri + '>';

    return this.query(query, 'update');
  }

  query(query, queryType) {
    if (!queryType) queryType = 'query';
    const path = this.apiPath + this.dataset + '/' +
      queryType + '?' + queryType + '=' + query;

    try {
      let result = HTTP.call('POST', path, {
        headers: {
          'Accept': 'application/sparql-results+json; charset=utf-8',
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        },
      });
      if (!result || !result.content)
        throw new Meteor.Error(400, 'Error 400: your query did not return anything');
      if (result.statusCode === 200) {
        if (queryType === 'query') return JSON.parse(result.content).results.bindings;
        else return result.statusCode;
      }
      else throw new Meteor.Error(result.statusCode, result.content);
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  construct(query) {
    const path = this.apiPath + this.dataset + '/query?query=' + query;

    try {
      let result = HTTP.call('POST', path, {
        headers: {
          'Accept': 'text/turtle; charset=utf-8',
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        },
      });
      if (!result || !result.content)
        throw new Meteor.Error(400, 'Error 400: your query did not return anything');
      if (result.statusCode === 200) return result.content;
      else throw new Meteor.Error(result.statusCode, result.content);
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  addDataToGraph(graphName, graphData) {
    const path = this.apiPath + this.dataset + '/data' + '?graph=' + graphName;

    try {
      let result = HTTP.call('POST', path, {
        content: graphData,
        headers: {
          'Content-Type': 'text/turtle; charset=utf-8',
        },
      });
      if (!result || !result.content)
        throw new Meteor.Error(400, 'Error 400: your query did not return anything');
      if (result.statusCode === 200 || result.statusCode === 201) return true;
      else throw new Meteor.Error(result.statusCode, result.content);
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  getGraph(graphName) {
    const path = this.apiPath + this.dataset + '/data' +
      '?graph=' + graphName;

    try {
      let result = HTTP.call('GET', path, {
        headers: {
          'Accept': 'text/turtle; charset=utf-8',
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        },
      });
      if (!result || !result.content)
        throw new Meteor.Error(400, 'Error 400: your query did not return anything');
      if (result.statusCode === 200) return result.content;
      else throw new Meteor.Error(result.statusCode, result.content);
    } catch (error) {
      if (error.response.statusCode !== 404) console.log(error);
      return false;
    }
  }

  objectId() {
    var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
    return timestamp + 'xxxxxxxxxxxxxxxx'
      .replace(/[x]/g, () => (Math.random() * 16 | 0).toString(16)).toLowerCase();
  }

  get dataset() {
    return this._dataset;
  }

  set dataset(value) {
    this._dataset = value;
  }

  get apiPath() {
    return this._apiPath;
  }

  set apiPath(value) {
    this._apiPath = value;
  }
}
