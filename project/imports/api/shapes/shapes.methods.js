
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { ShapesGraphs } from '/imports/api/shapes/shapes.class.js';

Shapes = new ShapesGraphs(FUSEKI_DATASET, FUSEKI_URL);

Meteor.methods({
  'shapes.find'(graphName) {
    check(graphName, String);

    return Shapes.getGraph(graphName);
  },
  'shapes.findCreatedAt'(graphName, timestamp) {
    check(graphName, String);
    check(timestamp, Date);

    return Shapes.getCreatedAt(graphName, timestamp);
  },
  'shapes.fetchHistory'(graphName) {
    check(graphName, String);

    return Shapes.getHistory(graphName);
  },
  'shapes.addData'(graphName, graphData) {
    check(graphName, String);
    check(graphData, String);

    return Shapes.add(graphName, graphData);
  },
  'shapes.createGraph'(graphName) {
    check(graphName, String);

    Shapes.createGraph(graphName);
  },
  'shapes.findLatestTimestamp'(graphName) {
    check(graphName, String);

    return Shapes.getLatestTimestamp(graphName);
  },
});
