
import { GraphDatabase } from '/imports/api/graphDb.class.js';

export class ShapesGraphs extends GraphDatabase {
  add(graphName, graphData) {
    let timestamp = new Date().getTime();
    const data = this.prepareDataForAddition(graphName, graphData, timestamp);
    return this.addDataToGraph(graphName, data) ? timestamp : null;
  }

  prepareDataForAddition(graphName, graphData, timestamp) {
    let graph = RDFLib.graph();

    try {
      RDFLib.parse(graphData, graph, graphName, 'text/turtle');
      graph.each(undefined, RDF('type'), VIZ('Shape')).forEach(node => {
        if (!graph.any(node, RDF('ID'), undefined))
          graph.add(node, RDF('ID'), SHAPE('s_' + this.objectId()));
        graph.add(node, VIZ('timestamp'), timestamp);
      });
      return graph.statements.join('');
    } catch (err) {
      console.log(err);
    }
  }

  getHistory(graphName) {
    // TODO : prevent injections
    let query =
      'PREFIX viz: <' + VIZ().value + '> ' +
      'SELECT DISTINCT ?timestamp ' +
      'FROM <' + graphName + '> ' +
      'WHERE { ' +
      '  ?subject viz:timestamp ?timestamp .' +
      '} ' +
      'ORDER BY DESC(?timestamp)';

    return this.query(query);
  }

  getCreatedAt(graphName, timestamp) {
    // TODO : prevent injections
    let query =
      'PREFIX viz: <' + VIZ().value + '> ' +
      'CONSTRUCT { ?s ?p ?o } ' +
      'FROM <' + graphName + '> ' +
      'WHERE { ' +
      '  ?s viz:timestamp ' + timestamp.getTime() + ' . ' +
      '  ?s ?p ?o . ' +
      '}';

    return this.construct(query);
  }

  getLatestTimestamp(graphName) {
    let query =
      'PREFIX viz: <' + VIZ().value + '> ' +
      'SELECT DISTINCT ?timestamp ' +
      'FROM <' + graphName + '> ' +
      'WHERE { ' +
      '  ?subject viz:timestamp ?timestamp .' +
      '} ' +
      'ORDER BY DESC(?timestamp) ' +
      'LIMIT 1';

    const result = this.query(query);

    if (result.length) return result[0].timestamp.value;
    return null;
  }
}
