
import { FilesCollection } from 'meteor/ostrio:files';

export const Images = new FilesCollection({
  collectionName: 'Images',
  allowClientCode: true,
  onBeforeUpload(file) {

    if (this.connection) Security.check(['admin', 'manager', 'commercial']);

    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 10485760 && /png|jpg|jpeg/i.test(file.extension)) {
      return true;
    } else {
      return 'Please upload image, with size equal or less than 10MB';
    }
  },
  onAfterUpload(file) {
    Meteor.call('shapes.createGraph', GRAPH('g_' + file._id).value);
  },
  storagePath: 'assets/uploads',
});
