
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Images } from '/imports/api/images/images.js';

Meteor.methods({
  'images.remove'(imageId) {
    check(imageId, String);

    Images.remove({_id: imageId});
  },
});
